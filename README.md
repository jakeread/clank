# Clank

Clank machines are straightforwards, easy to fabricate and assemble machine platforms. They are 'mostly printed' on off the shelf FDM printers, use roller bearings as linear guides on off the shelf aluminum extrusion, and standard NEMA 17 motors driving GT2 belts. 

The *-LZ* variant (below) was used in [HTMAA 2020](https://fab.cba.mit.edu/classes/863.20/) as a kit for remote students (one machine per student) and the *-CZ* variant is my current baby, where I am trying to integrate multi-process machining & a suite of other tricks. 

[dev log is here](log/clank-log.md)  

## [Clank-LZ](https://gitlab.cba.mit.edu/jakeread/clank-lz)

Documentation for the [Clank-LZ](https://gitlab.cba.mit.edu/jakeread/clank-lz) variant, that was used for HTMAA 2020 has been moved into a stable, standalone project [here](https://gitlab.cba.mit.edu/jakeread/clank-lz).

![clank](log/images/2020-07-30_clank-lz-fab-01.jpg)

## [Clank-FXY](https://gitlab.cba.mit.edu/jakeread/clank-fxy)

This is a larger machine, **256x256x128mm**, that uses a taller frame and a cantelivered bed that moves in the Z direction. This platform would be appropriate for jobs with large Z works spaces, i.e. 3D Printing, but also suited for CNC Milling of circuits, molds, components etc. 

![render](log/images/2021-04-30_multi-large.png)

## [Clank-Stretch](https://gitlab.cba.mit.edu/jakeread/clank-stretch)

A wide & thin aspect ratio, clank-stretch uses a small, short-throw z-axis. Appropriate for knife cutting, pen plotting, sheet milling, pick-and-placing, etc. 

![stretch](log/images/2021-02-20_clank-stretch.jpg)

## Tool Changing: [Hotplate](https://gitlab.cba.mit.edu/jakeread/hotplate)

I am bringing a [toolchanger system](https://gitlab.cba.mit.edu/jakeread/hotplate) on line for this as well, small demo below, design is not yet public. 

![toolchange](log/videos/2021-02-12_hot-pinch.mp4)

## HDPE Milling

![milling](log/videos/2021-01-05_clank-mills-hdpe.mp4)

## Printing

![first print](log/videos/2021-01-24_hotend-print.mp4)
