## Clank-LZ BOM 

The Clank-LZ (small circuit milling) variant's BOM is tracked on google sheets, link below:

[BOM Here](https://docs.google.com/spreadsheets/d/1HzgdPIvii0NI3s8gkg9xFz3LI2AqHLVXkGTv0clRpXw/edit?usp=sharing)

## Clank-CZ BOM 

The Clank-CZ BOM (a larger, more versatile machine) is still roughing out, and documented below. 

| HW | Where |
| --- | --- |
| M4 Nylocks | Everywhere |
| M4 Washers | - |
| M4 FHCS x 40 | Thru Bottom / Motor / Top Plates Stack |
| M4 FHCS x 25 | YLB, YLF, YRB, YRF Belt Pinch | 
| M4 BHCS x 25 | Thru Face Plate 'ears' / Rollers / Top & Bottom Plates |
| M4 BHCS x 25 | Y Left Motor Plate to Top Plate | 
| M4 FHCS x 30 | Thru Z Drive Shells | 
| M4 BHCS x 14 | Thru Roller / Washer / Plate on sindle-sided rollers | 
| M5 Nylocks | - |
| M5 Washers | - | 
| M5 FHSC x 10 | Tapped-End Extrusion -> Parts | 
| M5 FHCS x 15 | Z-Drive to Bed | 4 | 
| M5 SHCS x 25 | Z-Bed Thru 625s | 
| M5 FHCS x 50 | Z-Drive Thru Idlers | 3 | 

| Cable HW | PN | 
| --- | --- | 
| 0.047" (3/64") Diameter Wire Rope | 3461T632 |
| 3/64" Diameter Wire Rope Stop | 3926T43 |
| 1/16" ID 1/8" OD PTFE Tube | 5239K24 | 
| Compression Tool | 3582T5 |

| Part | Where | QTY |
| --- | --- | --- |
| 624 Bearings | Rollers | 44 |
| 625 Bearings | Clutch Bed Rollers | 4 |
| 6806 Bearings | Bed Belt Reduction | 2 | 
| 20T 10mm Wide GT2 Pulleys | Motion | 4 |
| 10mm Wide Open GT2 Belt | - | ~ 1m ? |
| 6mm Wide Closed GT2 Belt 280mm Circ | Z Reduction | 1 |

| Wiring | PN | Description |
| --- | --- | --- |
| Guide Hose | 5140K134 | 4mm ID, 6mm OD High Pressure Nylon Tubing |

| Part | Misumi PN | Count | 
| --- | --- | --- |
| 5-Series Tabbed Brackets | HBLFSN5-SET | 41 |
| M5 5-Series Extrusion T-Nuts | HNTT5-5 | 4 |
| X Beam | HFS5-2060-366-TPW | 1 | 
| Y Beams, Frame Tops | HFS5-2020-442-TPW | 4 |
| Frame Bottom Sides | HFS5-2020-411-TPW | 2 |
| Frame Cross Members | HFS5-2020-381 | 5 | 
| Frame Corner Posts | HFS5-2020-290 | 6 | 
| Bed Posts | HFS5-2020-108 | 2 | 
| Bed Cross Members | HFS5-2020-188-TPW | 3 | 
| Bed Y Dir | HFS5-2020-259 | 2 | 
| Bed Struts | HFS5-2020-244-TPW | 2 | 
