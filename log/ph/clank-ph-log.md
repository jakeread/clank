## 2020 10 29 

This morning I'm after a thought w/r/t making clank gantries / parts largely with phenolic sheet milled on the zund. This is almost entirely about wanting to make it stiffer - way stiffer. Trying to pull up on zund type performance. 

![start](2020-10-29_ph-yl-01.png)

Starting out, this seems totally possible if I get into some epoxy layup for the tricky 90' joints. I also need to go flush on a handful of the hardware joints, and probably will have to go carefully about hardware lengths. 

At the moment my big question is about hardware: do I insert brass threads, or mill tiny pockets for hex nuts? I am worried about vibrating out of brass inserts, but probably lock-tite can undo that if machines need to become very reliable. 

Assembly will also be harder - mostly in the area between these two side plates, for the idlers... those washers have to pinch in between. But I think it's not impossible, and worth the stiffness. 

The other trick is w/r/t the flexures. Can I get away with milled phenolic flexures, which is not a very forgiving material in elongation? Or will I have to mount-in separate printed flexures? 

My last question is about what this is for: do I want this for better clank sized machines, or should I get out of that space, be satisfied with prints there, and use these design topologies for a 4x8 router? 

The brass inserts seem like they'll work well, I'll start with those. They're a bit expensive. 

![compare](2020-10-29_ph-compare-01.png)
![compare](2020-10-29_ph-compare-02.png)
![compare](2020-10-29_ph-compare-03.png)

The biggest change is the 'interior offset' - the motor extends farther here into the X Carriage zone. This is constrained by the location of the X Constraint's flexure, which prevents me from moving the idler into that geometry:

![start](2020-10-29_ph-yl-02.png)

But I think I can pinch the distance between the motor and the idlers... 

Ah, I can do better than the old design in this regard actually. 30mm against 36, that's great. 

I am a little weary of the increased distance from the motor flange to the action at the pulley, but I suspect the increased loads on the motor bearings are acceptable given the great improvement in overall stiffness. 

So, great, so far this is a lot better than the old clank design. It's totally machineable in really simple operations. I would say next up is a design for the x carriage, but this isn't contributing to any of my 'real work' at the moment, so I should probably shelf it / just daydream about it for now. 
