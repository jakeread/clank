## "Finished" Clank

I started this design about february of 2020, so they're ~ 1yr old now, although it only became an extrusion machine in March of 2020. 

I would like to, at some point, feel done with these designs. I think they fit a nice niche: being very easy to make, relatively robust for a few processes (print / light milling / pnp / pipetting / etc) this kind of 'desktop' scale. Being printed, they'll never be very stiff. However, making a series of tools and workflows available around this one platform seems valuable and the approachability of the scale is wonderful as well. Also, they're fairly cheap. 

So, before I allow myself to follow another path towards i.e. composite laid-up-beam machines (for i.e. a large format mill), I want to 'finish' this project. That would mean:

### Stable Toolchanger 

- this might get a cable-drive actuation, tbd if it helps in tool stability / tool changer weight / x-carriage size 
- otherwise, I think the form and fit of the changer is pretty good. 
- hot-swap network end effectors:
    - toolchanger wants a pogo-pin connector plate through to end effectors, so that each tool doesn't need its own wiring loom 

### XY Cable Routing

- this includes implementing / running power & network through to the tool changer, and the revisions should happen together 
- ideally, one routing solution for large & small machines 

### Clank-Print Z-Drive

- I am toying with the idea of lifting the XY gantries rather than dropping this heavy bed, TBD,

### Clank-Wide Z-Drive

- like the Clank-LZ, but better, I want a small z-drive that rides along with the X carriage for i.e. large plotters / milling machines / knife cutters / pnp (?) etc 

### Versioning

I think at 'the end' there would just be two variants: a wider / flat build, for "thin" aspect ratio fab, and a taller build for i.e. 3D Printing and small format milling (and the hybrids)

### Controllers

This also assumes success with OSAP, and there's a whole laundry list there as well. 

### Tools

- spindle
- probe 
- print head 
- camera 
- rotary / knife tool 
- ... etc: open season 

### Documentation

I would say the way to do this would be to do full documentation for 'wide' and 'tall' machines of some set size. Both should have interchangeable tools (and the same tool changer) so end-effector docs can be separate. 

So, in the end, a family of two machines with one set of interchangeable heads... and probably similar controllers, as well. 